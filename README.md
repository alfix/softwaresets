# SoftwareSets

**WARNING! - This project is obsolete a better utility is under development.**

*SoftwareSets* is a script with an User Interface to install software
collections on [FreeBSD](https://www.freebsd.org), the script is focused on
a Desktop/Laptop Environment.

I can't test: Trackpoint and i2c touchpads, NVIDIA and AMD GPUs.  
I can test: https://wiki.freebsd.org/Laptops/Acer_Aspire_5742G

Getting started

	% fetch https://gitlab.com/alfix/softwaresets/-/blob/master/softwaresets
	% chmod a+x softwareset
	% sudo ./softwareset


![screenshot](screenshot.png)

